SELECT * FROM country;

SELECT Code, Name, Continent, Region FROM country;

SELECT DISTINCT Continent FROM country;

SELECT * FROM country WHERE LifeExpectancy<=50;

SELECT * FROM country WHERE LifeExpectancy>=60;

SELECT * FROM country WHERE LifeExpectancy>=75 AND Population>=20000000;

SELECT * FROM country WHERE LifeExpectancy>=70 AND Population<=50000 ORDER BY LifeExpectancy;

SELECT * FROM country WHERE Name LIKE 'N%' OR Name Like 'S%';

SELECT MIN(LifeExpectancy) FROM country;

SELECT MAX(LifeExpectancy) FROM country;

SELECT continent, COUNT(Name) FROM Country GROUP BY continent;

SELECT AVG(LifeExpectancy) FROM country;

SELECT AVG(LifeExpectancy) FROM country WHERE continent = 'africa';